## Satyr: A livestreaming server

System dependencies: A stable version of node>=10, mysql3 (or a compatible implementation such as MariaDB), and ffmpeg >=4.2

### Build Instructions
```bash
git clone https://gitlab.com/knotteye/satyr.git
cd satyr
npm install
npm run setup
npm run build
```
Follow the instructions after setup runs.

### Run the server
```bash
npm start
```
